package steps;

import All.All_Test;
import Pages.InnerPage;
import Pages.StartPage;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;

import All.Browser;
import All.DriverProvider;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Cucumber_All_test {

    WebDriver driver;
    WebDriverWait wait;
    public static String URL = "https://www.google.com/intl/ru/gmail/about/";
    public static final String FireFoxPath = "C:\\Driver\\geckodriver.exe";
    public static final String ChromePath = "C:\\Driver\\chromedriver.exe";

    private String SendTo = null;// = "man.laughing@mail.ru";
    private String Subj = null;  // = "Тема письма созданного автоматически";
    private String Body = null;  // = "Тело сообщения, созданного автоматизированно";

    //Объявляем логгер событий
    private static final Logger log = Logger.getLogger(StartPage.class);

    @Before //Правильные Аннотации ! Не @BeforeTest как в TestNG И правильный импорт
    public void start(){
        this.driver = DriverProvider.getDriver(Browser.FIREFOX, FireFoxPath);
        this.wait = new WebDriverWait(driver, 10);
        this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        this.driver.manage().window().maximize();
//      this.Subj = this.generate(7);
//      this.Body = this.generate(7);
        this.Subj = UUID.randomUUID().toString();
        this.Body = UUID.randomUUID().toString();
    }

    @After //Правильные Аннотации ! Не @AfterTest как в TestNG И правильный импорт
    public void tearDown(){
        DriverProvider.quitDriver(Browser.FIREFOX);
        //this.driver.quit();
    }

    @Given("^Google Start Page '(.+)'")
    public void set_page (String URL) {
        Cucumber_All_test.URL=URL;
    }

    @And("^login to mail with '(.+)' using '(.+)'")
    public void loginTest(String mail, String password){

        // Логирование действий
        log.debug(">>>>>  Login to Gmail Test Started >>>>\r\n");

        StartPage c = new StartPage(this.driver);
        Assert.assertTrue(c.login(Cucumber_All_test.URL, mail, password));
    }

    @And("create random mail for '(.+)'") //
    public void NewMail(String SendTo) {
        InnerPage c = new InnerPage(this.driver);
        this.SendTo = SendTo;

        // Логирование действий
        log.info(">>>>>  Create New Message Test Started >>>>\r\n");

        Assert.assertTrue(c.NewMail(SendTo, Subj, Body));
    }

    @And("check mail for in draft and send this")
    public void Check_and_Send_Mail() {
        InnerPage c = new InnerPage(this.driver);

        // Логирование действий
        log.info(">>>>>  Find Mail in Draft and Send Test Started >>>>\r\n");

        Assert.assertTrue(c.checkdraft( SendTo,  Subj,  Body, true));
    }

    @And("check mail for in after send")
    public void Final_Check_Mail() {

        // Логирование действий
        log.info(">>>>>  Find Mail and Send Test Started >>>>\r\n");

        InnerPage c = new InnerPage(this.driver);
        //Письмо появилось (есть) в отправленных
        Assert.assertTrue(c.checksend( SendTo,  Subj,  Body, false));

        // Логирование действий
        log.info(" Check Message in Send = Found - OK ");

        InnerPage r = new InnerPage(this.driver);
        //Письма нет в черновиках
        Assert.assertFalse(r.checkdraft( SendTo,  Subj,  Body, false));

        // Логирование действий
        log.info(" Check Message in Draft = NOT Found - OK ");
    }

    @And("logout of this mail")
    public void logoutTest(){
        InnerPage c = new InnerPage(this.driver);

        // Логирование действий
        log.info(">>>>>  LogOut Test Started >>>>\r\n");

        Assert.assertTrue(c.log_out());
    }

    public String generate (int n) {
        String validChars = "abcdefghijklmnopqrstuvwxyzABCEDFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuffer ith = new StringBuffer();
        for (int i=0; i<n; i++) {
            ith.append(validChars.charAt(0+(int)(Math.random()*62)));
        }
        return ith.toString();
    }
}
