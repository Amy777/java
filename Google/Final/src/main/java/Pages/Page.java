package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public abstract class Page
{
    protected WebDriver driver;

    private WebElement webElement;

    public Page(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    protected void elementWait (WebElement webElement){
        WebElement el = new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(webElement));
    }
}
