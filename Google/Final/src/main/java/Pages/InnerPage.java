package Pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class InnerPage extends Page {

    public WebElement getGenbtn() {
        return genbtn;
    }


    //Кнопка с действиями аккаунта
    @FindBy(xpath = "//*[@id=\"gb\"]/div[1]/div[1]/div[2]/div[5]/div[1]/a/span")
    protected WebElement genbtn;

    //Кнопка "Выход"
    @FindBy(css = "#gb_71")
    protected WebElement exitbtn;

    public WebElement getDraftbtn() {
        return draftbtn;
    }

    //Папка Черновики     /html/body/div[7]/div[3]/div/div[2]/div[1]/div[1]/div[1]/div[2]/div/div/div/div[2]/div/div[1]/div[1]/div/div[4]/div/div/div[2]/span/a
    @FindBy(xpath = "//div[4]/div/div/div[2]/span/a[@href='https://mail.google.com/mail/#drafts']")
    protected WebElement draftbtn;

    //Папка Отправленные  /html/body/div[7]/div[3]/div/div[2]/div[1]/div[1]/div[1]/div[2]/div/div/div/div[2]/div/div[1]/div[1]/div/div[3]/div/div/div[2]/span/a
    @FindBy(xpath = "//div[3]/div/div/div[2]/span/a[@href='https://mail.google.com/mail/#sent']")
    protected WebElement sendbtn;

    public InnerPage(WebDriver driver) {
        super(driver);
    }

    public boolean NewMail(String SendTo, String Subj, String Body) {
        NewMSGPage x = new NewMSGPage(this.driver);
        return (x.write_msg(SendTo, Subj, Body));
    }

    //Объявляем логгер событий
    private static final Logger log = Logger.getLogger(StartPage.class);

    public boolean log_out () {
        //Выход из почты
        //Ожидаем кнопку вверху для аккаунта

        //Логирование событий
        log.info("Logout Started");

        elementWait(genbtn); // wait.until(visibilityOfElementLocated(By.xpath("//*[@id=\"gb\"]/div[1]/div[1]/div[2]/div[5]/div[1]/a/span")));
        //Нажимаем на нее
        genbtn.click();

        //Логирование событий
        log.info("Account btn is pressed");

        InnerPage x = new InnerPage(this.driver);
        //Ожидаем кнопку "Выход"
        elementWait(x.exitbtn); // wait.until(visibilityOfElementLocated(By.cssSelector("#gb_71")));
        //Нажимаем на нее
        x.exitbtn.click();

        //Логирование событий
        log.info("Exit btn is pressed");

        try {
            Alert alt = driver.switchTo().alert();
            alt.accept();
            alt.accept();

            //Логирование событий
            log.info("Browser accept windows");

        } catch (Exception ex) {

        }
        finally {
            //Проверяем выход
            StartPage y = new StartPage(this.driver);
            //Ожидаем окно ввода пароля
            elementWait(y.getPasswfld()); // wait.until(visibilityOfElementLocated(By.cssSelector("#password input[name=password][type=password]")));
            if (y.getPasswfld().isEnabled()) {

                //Логирование событий
                log.info("Logout is OK");

                return true;
            } else {

                //Логирование событий
                log.error("Logout is NOT OK");

                return false;
            }
        }
    }

    //WebElement - куда переходим, возвр. значение - проверяем есть или нет это письмо, send - отправлять или нет письмо, если найдено
    // Возвращает, найдено или нет письмо
    public boolean checkmail (String SendTo, String Subj, String Body, WebElement xc, boolean send) {
        //Ожидаем элемент
        elementWait(xc); // wait.until(visibilityOfElementLocated(By.xpath(folder)));
        //Нажимаем на нее, переходим в соотвестствующую папку
        xc.click();

        DraftSendPage c = new DraftSendPage(this.driver, xc);
        return c.CheckMail(SendTo, Subj, Body, send);
    }

    public boolean checkdraft (String SendTo, String Subj, String Body, boolean send) {
        //Логирование событий
        log.info("Check Drafts");
        return this.checkmail(SendTo, Subj, Body, this.draftbtn, send);
    }

    public boolean checksend (String SendTo, String Subj, String Body, boolean send) {
        //Логирование событий
        log.info("Check Sends");
        return this.checkmail(SendTo, Subj, Body, this.sendbtn, send);
    }
}



// Рабочий метод создания нового письма
/*
    public void NewMail() {
        //Ожидаем появления кнопки "НАПИСАТЬ"
        wait.until(visibilityOfElementLocated(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[1]/div[1]/div[2]/div/div/div/div/div/div"))); //
        //Находим как WebElement
        WebElement writeText = driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[1]/div[1]/div[2]/div/div/div/div/div/div"));
        //Нажимаем на нее
        writeText.click();

        //Ждем окна ввода нового сообщения

        //Ожидаем появления кнопки "Отправить" нового сообщения  /html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/table/tbody/tr[2]/td/div/div/div[4]/table/tbody/tr/td[1]
        wait.until(visibilityOfElementLocated(By.xpath("/html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/table/tbody/tr[2]/td/div/div/div[4]/table/tbody/tr/td[1]/div/div[2]"))); //

        //Находим как WebElement
        //Если еще не кликнуто "Получатели"
        if (!driver.findElements(By.xpath("/html/body/div[17]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/div[2]")).isEmpty()) {
        WebElement SendField = driver.findElement(By.xpath("/html/body/div[17]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/div[2]"));
        //Нажимаем на нее
        SendField.click();
        }
        //Получаем поле для ввода
        WebElement SendToField = driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/div[1]/table/tbody/tr[1]/td[2]/div/div/textarea"));
        //Нажимаем на нее
        //Вводим получателей //Actions.
        SendToField.click();
        SendToField.sendKeys(SendTo);

        //Ожидаем появления поля "Тема"
        wait.until(visibilityOfElementLocated(By.xpath("/html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/div[3]/input")));
        //Находим как WebElement
        WebElement SubjField = driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/div[3]/input"));
        //Нажимаем на нее
//      SubjField.click();
        //Вводим получателей
        SubjField.sendKeys(Subj);

        //Ожидаем появления поля "Сообщение"
        wait.until(visibilityOfElementLocated(By.xpath("/html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/div[1]/div[2]/div[1]/div/table/tbody/tr/td[2]/div[2]/div")));
        //Находим как WebElement
        WebElement BodyField = driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/div[1]/div[2]/div[1]/div/table/tbody/tr/td[2]/div[2]/div"));
        //Нажимаем на нее
        BodyField.click();
        //Вводим тело сообщения
        BodyField.sendKeys(Body);

        //Ожидаем
        wait.until(visibilityOfElementLocated(By.xpath("/html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[2]/div/div[2]/div/div/div/div/table/tbody/tr/td[2]/img[3]")));
        //И закрываем это окно, сохраняя в черновиках
        WebElement SaveExitBtn = driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[2]/div/div[2]/div/div/div/div/table/tbody/tr/td[2]/img[3]"));
        SaveExitBtn.click();

        //Проверяем
        //Закрытие окна
        boolean x =driver.findElements(By.xpath("/html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[2]/div/div[2]/div/div/div/div/table/tbody/tr/td[2]/img[x]")).isEmpty();
        Assert.assertTrue(x);
        System.out.println("Письмо успешно создано");
 */

// Рабочий метод выхода

/*
    public void logoutTest(){
        //Выход из почты
        //Ожидаем кнопку вверху для аккаунта
        wait.until(visibilityOfElementLocated(By.xpath("//*[@id=\"gb\"]/div[1]/div[1]/div[2]/div[5]/div[1]/a/span")));
        //Находим как WebElement
        WebElement akkButton = driver.findElement(By.xpath("//*[@id=\"gb\"]/div[1]/div[1]/div[2]/div[5]/div[1]/a/span"));
        //Нажимаем на нее
        akkButton.click();
        //Ожидаем кнопку "Выход"
        wait.until(visibilityOfElementLocated(By.cssSelector("#gb_71")));
        //Находим как WebElement
        WebElement exitButton = driver.findElement(By.cssSelector("#gb_71"));
        //Нажимаем на нее
        exitButton.click();

        try {
            Alert alt = driver.switchTo().alert();
            alt.accept();
            alt.accept();
        } catch (Exception ex) {

        }
        finally {
            //Проверяем выход
            //Ожидаем окно ввода пароля
            wait.until(visibilityOfElementLocated(By.cssSelector("#password input[name=password][type=password]")));
            Assert.assertTrue(driver.findElement(By.cssSelector("#password input[name=password][type=password]")).isEnabled());
        }
}
*/