package Pages;

import Pages.InnerPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class NewMSGPage extends InnerPage {

    //Кнопка "НАПИСАТЬ"
    @FindBy(xpath="/html/body/div[7]/div[3]/div/div[2]/div[1]/div[1]/div[1]/div[2]/div/div/div/div[1]/div/div")
    protected WebElement writebtn;

    public WebElement getSendbtn1() {
        return sendbtn1;
    }

    //Кнопка "Отправить"
    //             /html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/table/tbody/tr[2]/td/div/div/div[4]/table/tbody/tr/td[1]/div/div[2]
    @FindBy(xpath="//div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/table/tbody/tr[2]/td/div/div/div[4]/table/tbody/tr/td[1]/div/div[2]")
    protected WebElement sendbtn1;

    //Лейбл "Получатели"
    @FindBys(@FindBy(xpath="/html/body/div[17]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/div[2]"))
    protected List<WebElement> sendlbl1;

    //Поле для ввода Получаетелей
    //             /html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/div[1]/table/tbody/tr[1]/td[2]/div/div/textarea
    @FindBy(xpath="//div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/div[1]/table/tbody/tr[1]/td[2]/div/div/textarea")
    protected WebElement sendfld;

    //Поле для ввода Темы //
    //             /html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/div[3]/input
    @FindBy(xpath="//div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/div[3]/input")
    protected WebElement subjfld;

    //Поле для ввода Тела сообщения
    //             /html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/div[1]/div[2]/div[1]/div/table/tbody/tr/td[2]/div[2]/div
    @FindBy(xpath="//div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/div[1]/div[2]/div[1]/div/table/tbody/tr/td[2]/div[2]/div")
    protected WebElement bodyfld;

    public WebElement getSvclbtn() {
        return svclbtn;
    }

    //Кнопка Закрыть и сохранить
    //             /html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[2]/div/div[2]/div/div/div/div/table/tbody/tr/td[2]/img[3]
    @FindBy(xpath="//div/div[2]/div/div[2]/div/div/div/div/table/tbody/tr/td[2]/img[3]")
    protected WebElement svclbtn;

    public List<WebElement> getMsgwnd() {
        return msgwnd;
    }

    //Общее окно сообщения
    //                      /html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]
    @FindBys(@FindBy(xpath="//div/div[3]/div/div/div[4]/table/tbody/tr/td[2]"))
    protected List<WebElement> msgwnd;

    //Поле для отображения Получаетелей
    //             /html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/div[2]/div/span
    @FindBy(xpath="//div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/div[2]/div/span")
    protected WebElement sendflddrft;

    //Поле для отображения Темы
    //             /html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/input[@name='subject']
    @FindBy(xpath="//div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/input[@name='subject']")
    protected WebElement subjflddrft;

    public NewMSGPage(WebDriver driver) {
        super(driver);
    }

    //Объявляем логгер событий
    private static final Logger log = Logger.getLogger(StartPage.class);

    public boolean write_msg(String SendTo, String Subj, String Body) {

     //Логирование событий
     log.info("Write new message method");

    //Ожидаем появления кнопки "НАПИСАТЬ"
     elementWait(writebtn); // wait.until(visibilityOfElementLocated(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[1]/div[1]/div[2]/div/div/div/div/div/div"))); //
    //Нажимаем на нее
     writebtn.click();

    //Ждем окна ввода нового сообщения
    //Ожидаем появления кнопки "Отправить" нового сообщения  /html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/table/tbody/tr[2]/td/div/div/div[4]/table/tbody/tr/td[1]
    //elementWait(sendbtn1); // wait.until(visibilityOfElementLocated(By.xpath("/html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/table/tbody/tr[2]/td/div/div/div[4]/table/tbody/tr/td[1]/div/div[2]"))); //

    //Находим как WebElement
    //Если еще не кликнуто "Получатели"
        if (!sendlbl1.isEmpty()) {
        //Нажимаем на нее
        sendlbl1.get(0).click();
    }

        //Логирование событий
        log.info("Enter field SendTo");

    //Получаем поле для ввода Получаетелей
    // Нажимаем на нее
        sendfld.click();
    //Вводим получателей //Actions.
        sendfld.sendKeys(SendTo);

        //Логирование событий
        log.info("Enter field Subj");

    //Ожидаем появления поля "Тема"
    elementWait(subjfld); // wait.until(visibilityOfElementLocated(By.xpath("/html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/div[3]/input")));
    //Нажимаем на нее
//      subjfld.click();
    //Вводим получателей
        subjfld.sendKeys(Subj);

        //Логирование событий
        log.info("Enter field Body");

    //Ожидаем появления поля "Сообщение"
    elementWait(bodyfld); // wait.until(visibilityOfElementLocated(By.xpath("/html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/div[1]/div[2]/div[1]/div/table/tbody/tr/td[2]/div[2]/div")));
    //Нажимаем на нее
        bodyfld.click();
    //Вводим тело сообщения
        bodyfld.sendKeys(Body);

        //Логирование событий
        log.info("Click Save&Exit");

    //Ожидаем кнопку закрытия
    elementWait(svclbtn); // wait.until(visibilityOfElementLocated(By.xpath("/html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[2]/div/div[2]/div/div/div/div/table/tbody/tr/td[2]/img[3]")));
    //И закрываем это окно, сохраняя в черновиках
        svclbtn.click();

    //Проверяем
    //Закрытие окна - не лейбла Получатели
    InnerPage s = new InnerPage(this.driver);
    boolean x = sendlbl1.isEmpty();
    //Assert.assertTrue(x);
        if (x) {
            //Логирование событий
            log.info("Message create Successful (OK)");
           //System.out.println("Письмо успешно создано");
        } else {
            //Логирование событий
            log.error("Message NOT create successful");
           //System.out.println("Ошибка, Письмо НЕ создано");
        }
        return x;
}

public boolean check_msg (String SendTo, String Subj, String Body) {
    if ((SendTo.equalsIgnoreCase(sendflddrft.getText()))&&(Subj.equalsIgnoreCase(subjflddrft.getAttribute("value")))&&(Body.equalsIgnoreCase(bodyfld.getText()))) {
        return true;
    } else {
        return false;
    }
}
}
