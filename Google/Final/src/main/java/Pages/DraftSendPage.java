package Pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class DraftSendPage extends InnerPage {

    //Список сообщений - строки целиком (на всякий случай)
    //               /html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[2]/div[3]/div[1]/div/table/tbody - черновики
    //               /html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[3]/div[3]/div[1]/div/table/tbody - входящие
    @FindBy(xpath = "//div[3]/div[1]/div//table/tbody")
    protected WebElement msgtable;

    //Список сообщений - строки целиком для черновиков
    @FindBys(@FindBy(xpath = "//div[2]/div[3]/div[1]/div/table/tbody//tr"))
    protected List<WebElement> draftmsglist;

    //Список сообщений - строки целиком лля отправленных
    @FindBys(@FindBy(xpath = "//div[3]/div[3]/div[1]/div/table/tbody//tr"))
    protected List<WebElement> sendmsglist;

    protected List <WebElement> msglist;

    //Чтоб не плодить веб элементы отдельно для черновиков и отправленных
    //Список - тем сообщений (Селектор)
    By subjlist = By.xpath("./td[6]/div/div/div/span[1]");

    //Список - тем сообщений
    By bodylist = By.xpath("./td[6]/div/div/div/span[2]");

/*  //Список - список тем сообщений
    @FindBys(@FindBy(xpath = "//div[3]/div[1]/div/table/tbody/tr//td[6]/div/div/div/span[1]"))
    protected List<WebElement> subjmsglist;

    //Список - список тел сообщений
    @FindBys(@FindBy(xpath = "//div[3]/div[1]/div/table/tbody/tr//td[6]/div/div/div/span[2]"))
    protected List<WebElement> bodymsglist;
*/
    //Тело сообшения, открытого в главном окне
    //
    //               /html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[4]/div/table/tr/td[1]/div[2]/div[2]/div/div[3]/div[1]/div/div/div/div/div[1]/div[2]/div[5]/div/div[1]
    @FindBy(xpath = "//div[4]/div/table/tr/td[1]/div[2]/div[2]/div/div[3]/div[1]/div/div/div/div/div[1]/div[2]/div[5]/div/div[1]")
    protected WebElement bodymsggenwnd;

    //Получатели сообшения, открытого в главном окне
    //               /html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[4]/div/table/tr/td[1]/div[2]/div[2]/div/div[3]/div[1]/div/div/div/div/div[1]/div[2]/div[1]/table/tbody/tr[2]/td/table/tbody/tr/td/div[1]/span/span
    @FindBy(xpath = "//div[4]/div/table/tr/td[1]/div[2]/div[2]/div/div[3]/div[1]/div/div/div/div/div[1]/div[2]/div[1]/table/tbody/tr[2]/td/table/tbody/tr/td/div[1]/span/span")
    protected WebElement sendmsggenwin;

    //Тема сообшения, открытого в главном окне
    //               /html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[4]/div/table/tr/td[1]/div[2]/div[1]/div[2]/div[1]/h2
    @FindBy(xpath = "//div[4]/div/table/tr/td[1]/div[2]/div[1]/div[2]/div[1]/h2")
    protected WebElement subjmsggenwin;

    //Кнопка отправить сообшения, открытого в главном окне
    //               /html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[3]/div/table/tr/td[1]/div[2]/div[2]/div[1]/div[3]/div/div/div/div/div/div[2]/div/div/div/div[4]/table/tbody/tr/td[2]/table/tbody/tr[2]/td/div[2]/div[1]/div[4]/table/tbody/tr/td[1]/div/div[2]
    @FindBy(xpath = "//div[3]/div/table/tr/td[1]/div[2]/div[2]/div[1]/div[3]/div/div/div/div/div/div[2]/div/div/div/div[4]/table/tbody/tr/td[2]/table/tbody/tr[2]/td/div[2]/div[1]/div[4]/table/tbody/tr/td[1]/div/div[2]")
    protected WebElement sendbtnmsggenwin;

    //Кнопка вернуться к списку сообшения, открытого в главном окне
    @FindBy(xpath = "/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[1]/div/div[1]/div/div[1]/div/div/div")
    protected WebElement returnbtn;

    private WebElement fldr = null;

    //Объявляем логгер событий
    private static final Logger log = Logger.getLogger(StartPage.class);

    public DraftSendPage(WebDriver driver, WebElement fld) {
        super(driver);
        fldr=fld;
        if (fld.equals(this.draftbtn)) {
            msglist=draftmsglist;
        } if (fld.equals(this.sendbtn)) {
            msglist=sendmsglist;
        }
    }

    public boolean CheckMail (String SendTo, String Subj, String Body, boolean send) {
        //Проверяем наличие письма
        //Ожидаем
        elementWait(msgtable); // wait.until(visibilityOfElementLocated(By.xpath(xpath)));
        //Идем по списку и проверяем сначала совпадение кусочка темы и кусочка тела
        for (int i=0; i<msglist.size(); i++ ) {

            //Логирование событий
            log.info("Check MailList");

            DraftSendPage y = new DraftSendPage(this.driver, fldr);
            if (y.msglist.size()<i) { // Если список уменьшился и проверять нечего, (в результате действий другого пользователя напр.)
                //Логирование событий
                log.error("Error to multithread");
                return false;
            }
            //Если есть кусок темы и тела, заходим в письмо, проверяем остальное
            if ((y.msglist.get(i).findElement(subjlist).getText().contains(Subj.substring(0,5)))&&(y.msglist.get(i).findElement(bodylist).getText().contains(Body.substring(0,5)))) {
                //Нажимаем на сообщение
                y.msglist.get(i).findElement(subjlist).click();
                //Смотрим открытое сообщние и где оно открылось
                //Если открылось в окне
                NewMSGPage c = new NewMSGPage(this.driver);
                if (!c.getMsgwnd().isEmpty()) {
                    //Логирование событий
                    //log.info("Check mail in windows");
                    if (c.check_msg(SendTo, Subj, Body)) { //Если письмо соответствует критериям ...
                        //Логирование событий
                        log.info("Mail Find OK");
                            if (send) { //Если надо отправить
                                c.getSendbtn1().click(); // Отправляем его (ОПЦИОНАЛЬНО)
                                //Логирование событий
                                log.info("Mail is send");
                                //System.out.println("Проверка успешно завершена, письмо отправлено");
                            return true;
                        } //Если проверяем отправленные и нашли, то нормально

                    } else {
                        c.getSvclbtn().click(); //Если не то сообщение, закрываем окно с ним
                    }
                } else { //Если открылось в главном окне
                    // Ожидание
                    DraftSendPage x = new DraftSendPage(this.driver, fldr);
                    elementWait(x.bodymsggenwnd); // wait.until(visibilityOfElementLocated(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[3]/div/table/tr/td[1]/div[2]/div[2]/div/div[3]/div/div/div/div/div/div[1]/div[2]/div[5]/div/div[1]")));
                    //Логирование событий
                    //log.info("Check mail in general windows");
                    if ((SendTo.equalsIgnoreCase(x.sendmsggenwin.getAttribute("email")))&&(Subj.equalsIgnoreCase(x.subjmsggenwin.getText()))&&(Body.equalsIgnoreCase(x.bodymsggenwnd.getText()))) { //Если письмо соответствует критериям ...
                        //Логирование событий
                        log.info("Mail Find OK");
                        if (send) { //Если надо отправить
                            x.sendbtnmsggenwin.click(); // Отправляем его (ОПЦИОНАЛЬНО)
                            //Логирование событий
                            log.info("Mail is send");
                            //System.out.println("Проверка успешно завершена, письмо отправлено");
                        } //Если проверяем отправленные и нашли, то нормально
                        return true;
                        } else {
                            x.returnbtn.click(); //Это не то письмо, возвращаемся на уровень выше
                        }
                }
            }
        }
        //Логирование событий
        log.info("Mail NOT Found");
        return false;
    }

}
