package Pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.*;

public class StartPage  extends Page {

    public StartPage(WebDriver driver) {
        super(driver);
    }

    //Кнопка "Вход на стартовой странице"
    @FindBy(css ="body > nav > div > a.gmail-nav__nav-link.gmail-nav__nav-link__sign-in")
    protected WebElement inBtn;

    //Окно выбора аккаунта или авторизации
    @FindBy(css ="div [role=presentation]")
    protected WebElement authwind;

    //Окно выбор аккаунта
    @FindBy(css ="#headingText")
    protected WebElement chakk;

    @FindBy(id ="#identifierLink div p")
    protected WebElement lastakk;

    @FindBy(id ="#identifierLink div p")
    protected WebElement lastakkd;

    //Поле ввода логина
    @FindBy(id ="identifierId")
    protected WebElement loginfld;

    //Кнопка далее на окне логин
    @FindBy(css="#identifierNext > content:nth-child(3) > span:nth-child(1)")
    protected WebElement forvbtn1;

    public WebElement getPasswfld() {
        return passwfld;
    }

    //Поле ввода пароля
    @FindBy(css="#password input[name=password][type=password]")
    private WebElement passwfld;

    //Кнопка далее на окне пароля
    @FindBy(css="#passwordNext > content > span")
    private WebElement forvbtn2;

    //Объявляем логгер событий
    private static final Logger log = Logger.getLogger(StartPage.class);

    //Из-за того, что при использовании аннотации, веб объекты инициализируются только при создании объекта (а там несколько переходов и элементы появляются динамически),
    // каждый раз приходится создавать объекты этих классов, или можно делить действия и создавать объекты в вызывающем методе, но это перегружает его

    public boolean login(String URL, String mail, String password ) {

        //Получаем URL входа гугла
        driver.get(URL);

        // Логирование действий
        log.info("Get Start Page in URL: "+ URL);

        //Ожидаем появления кнопки "Вход"
        elementWait(inBtn);  //   wait.until(visibilityOfElementLocated(inBtn));

        // Логирование действий
        log.info("Page is Download");

        //Нажимаем на нее
        inBtn.click();

        // Логирование действий
        log.info("Goto to authorized");

        //Если нам предлагают выбрать аккаунт
        StartPage z = new StartPage(this.driver);
        //Ожидаем окна выбора аккаунта или авторизации
        elementWait(z.authwind);    //       wait.until(visibilityOfElementLocated(By.cssSelector("div [role=presentation]")));
        if (z.chakk.isEnabled()) {
            if(z.chakk.getText().contains("Выберите аккаунт")){
                // Логирование действий
                log.debug("Choose account branch, new auth");
                z.lastakk.click();
            }
        }

        //Ввод логина
        StartPage y = new StartPage(this.driver);
        //Ожидаем поле ввода логина
        elementWait(y.loginfld); // wait.until(visibilityOfElementLocated(By.id("identifierId")));

        // Логирование действий
        log.info("Получено окно ввода почты или телефона");

        //Нажимаем, устанавливая курсор ввода
        y.loginfld.click();
        //Вводим свой логин (почта или телефон)
        y.loginfld.sendKeys(mail);

        // Логирование действий
        log.info("Введенная почта/телефон: "+mail);

        //Ожидаем кнопку "Далее" (на всякий случай)
        elementWait(y.forvbtn1); // wait.until(visibilityOfElementLocated(By.cssSelector("#identifierNext > content:nth-child(3) > span:nth-child(1)")));
        //Нажимаем на нее
        y.forvbtn1.click();


        //Ввод пароля
        StartPage x = new StartPage(this.driver);
        //Ожидаем окно ввода пароля
         elementWait(x.passwfld); // wait.until(visibilityOfElementLocated(By.cssSelector("#password input[name=password][type=password]")));

        // Логирование действий
        log.info("Получено окно ввода пароля");

        //Нажимаем на нее
//      passwfld.click();
        //Вводим свой пароль
        x.passwfld.sendKeys(password);
        //Ожидаем кнопку "Далее" на всякий случай )
        elementWait(x.forvbtn2); // wait.until(visibilityOfElementLocated(By.cssSelector("#passwordNext > content > span")));
        //Нажимаем на нее
        x.forvbtn2.click();

        //Проверяем условия входа в почту
        //Сверху есть кнопка с почтой и элементы
        InnerPage w = new InnerPage(this.driver);
        elementWait(w.getGenbtn()); // wait.until(visibilityOfElementLocated(By.xpath("//*[@id=\"gb\"]/div[1]/div[1]/div[2]/div[5]/div[1]/a/span"))); //By.cssSelector("a[title*="+mail+"][role=button][aria-expanded=true] span"))
//        Assert.assertTrue(w.getGenbtn().isEnabled());
        //Появились наприси "Черновики"
        elementWait(w.getDraftbtn()); // wait.until(visibilityOfElementLocated(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[1]/div[1]/div[2]/div/div/div/div[2]/div/div[1]/div[1]/div/div[4]/div/div/div[2]/span/a")));
//      Assert.assertTrue(w.getMail_wr().isEnabled());

        if ((w.getGenbtn().isEnabled())&&(w.getDraftbtn().isEnabled())) {

            // Логирование действий
            log.info("Авторизация успешно пройдена");

            return true;
        } else {

            // Логирование действий
            log.error("Авторизация провалена");

            return false;
        }
    }
}

//Старый рабочий метод из All.All_Test
/*
    public void loginTest(){
        //Получаем URL входа гугла
        driver.get(URL);
        //Ожидаем появления кнопки "Вход"
        wait.until(visibilityOfElementLocated(By.cssSelector("body > nav > div > a.gmail-nav__nav-link.gmail-nav__nav-link__sign-in")));
        //Находим как WebElement
        WebElement loginText = driver.findElement(By.cssSelector("body > nav > div > a.gmail-nav__nav-link.gmail-nav__nav-link__sign-in"));
        //Нажимаем на нее
        loginText.click();
        //Если нам предлагают выбрать аккаунт
        //Ожидаем окна выбора аккаунта или авторизации
        wait.until(visibilityOfElementLocated(By.cssSelector("div [role=presentation]")));
        if (driver.findElement(By.cssSelector("#headingText")).isEnabled()) {
            if(driver.findElement(By.cssSelector("#headingText")).getText().contains("Выберите аккаунт")){
                WebElement check = driver.findElement(By.id("#identifierLink div p"));
                check.click();
            }
        }
        //Ожидаем поле ввода логина
        wait.until(visibilityOfElementLocated(By.id("identifierId")));
        //Находим как WebElement
        WebElement loginField = driver.findElement(By.id("identifierId"));
        //Нажимаем, устанавливая курсор ввода
        loginField.click();
        //Вводим свой логин (почта или телефон)
        loginField.sendKeys(mail);
        //Ожидаем кнопку "Далее" (на всякий случай)
        wait.until(visibilityOfElementLocated(By.cssSelector("#identifierNext > content:nth-child(3) > span:nth-child(1)")));
        //Находим кнопку как WebElement
        WebElement forvButton1 = driver.findElement(By.cssSelector("#identifierNext > content:nth-child(3) > span:nth-child(1)"));
        //Нажимаем на нее
        forvButton1.click();
        //Ожидаем окно ввода
        wait.until(visibilityOfElementLocated(By.cssSelector("#password input[name=password][type=password]")));
        //Находим как WebElement
        WebElement passwField = driver.findElement(By.cssSelector("#password input[name=password][type=password]"));
        //Нажимаем на нее
//      passwField.click();
        //Вводим свой пароль
        passwField.sendKeys(password);
        //Ожидаем кнопку "Далее" на всякий случай )
        wait.until(visibilityOfElementLocated(By.cssSelector("#passwordNext > content > span")));
        //Находим как WebElement
        WebElement forvButton2 = driver.findElement(By.cssSelector("#passwordNext > content > span"));
        //Нажимаем на нее
        forvButton2.click();
        //Проверяем условия входа в почту
        //Сверху есть кнопка с почтой и элементы
        wait.until(visibilityOfElementLocated(By.xpath("//*[@id=\"gb\"]/div[1]/div[1]/div[2]/div[5]/div[1]/a/span"))); //By.cssSelector("a[title*="+mail+"][role=button][aria-expanded=true] span"))
        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"gb\"]/div[1]/div[1]/div[2]/div[5]/div[1]/a/span")).isEnabled());
        //Появились надписи о письмах

        wait.until(visibilityOfElementLocated(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[1]/div[1]/div[2]/div/div/div/div[2]/div/div[1]/div[1]/div/div[4]/div/div/div[2]/span/a")));
        Assert.assertTrue(driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[1]/div[1]/div[2]/div/div/div/div[2]/div/div[1]/div[1]/div/div[4]/div/div/div[2]/span/a")).isEnabled());
        }
  */