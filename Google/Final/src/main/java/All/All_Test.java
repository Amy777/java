package All;

import Pages.InnerPage;
import Pages.StartPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class All_Test {

    WebDriver driver;
    WebDriverWait wait;
    public static String URL = "https://www.google.com/intl/ru/gmail/about/";
    public static final String FireFoxPath = "C:\\Driver\\geckodriver.exe";
    public static final String ChromePath = "C:\\Driver\\chromedriver.exe";

    private String SendTo = "man.laughing@mail.ru";
    private String Subj; // = "Тема письма созданного автоматически";
    private String Body; // = "Тело сообщения, созданного автоматизированно";
    private String mail = "nshushakov777@gmail.com";
    private String password = "Z1!uC7fVaX";

    //Объявляем логгер событий
    private static final Logger log = Logger.getLogger(StartPage.class);

    @BeforeTest
    public void start(){
        this.driver = DriverProvider.getDriver(Browser.FIREFOX, FireFoxPath);
        this.wait = new WebDriverWait(driver, 10);
        this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        this.driver.manage().window().maximize();
//      this.Subj = All_Test.generate(7);
//      this.Body = All_Test.generate(7);
        this.Subj = UUID.randomUUID().toString();
        this.Body = UUID.randomUUID().toString();
    }

    @AfterTest
    public void tearDown(){
        DriverProvider.quitDriver(Browser.FIREFOX);
        //driver.quit();
    }

    @Test(priority=1)
    public void loginTest(){

        // Логирование действий
        log.debug(">>>>>  Login to Gmail Test Started >>>>\r\n");

        StartPage c = new StartPage(this.driver);
        Assert.assertTrue(c.login(All_Test.URL, mail, password));
    }

    @Test(dependsOnMethods="loginTest", priority=2) //
    public void NewMail() {
        InnerPage c = new InnerPage(this.driver);

        // Логирование действий
        log.info(">>>>>  Create New Message Test Started >>>>\r\n");

        Assert.assertTrue(c.NewMail(SendTo, Subj, Body));
    }

    @Test(dependsOnMethods="NewMail", priority=3)
    public void Check_and_Send_Mail() {
        InnerPage c = new InnerPage(this.driver);

        // Логирование действий
        log.info(">>>>>  Find Mail in Draft and Send Test Started >>>>\r\n");

        Assert.assertTrue(c.checkdraft( SendTo,  Subj,  Body, true));
    }

    @Test(priority=4)
    public void Final_Check_Mail() {

        // Логирование действий
        log.info(">>>>>  Find Mail and Send Test Started >>>>\r\n");

        InnerPage c = new InnerPage(this.driver);

        //Письмо появилось (есть) в отправленных
        Assert.assertTrue(c.checksend( SendTo,  Subj,  Body, false));

        // Логирование действий
        log.info(" Check Message in Send = Found - OK ");

        InnerPage r = new InnerPage(this.driver);
        //Письма нет в черновиках
        Assert.assertFalse(r.checkdraft( SendTo,  Subj,  Body, false));

        // Логирование действий
        log.info(" Check Message in Draft = NOT Found - OK ");
    }

    @Test(priority=5)
    public void logoutTest(){
        InnerPage c = new InnerPage(this.driver);

        // Логирование действий
        log.info(">>>>>  LogOut Test Started >>>>\r\n");

        Assert.assertTrue(c.log_out());
    }

    static String generate (int n) {
        String validChars = "abcdefghijklmnopqrstuvwxyzABCEDFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuffer ith = new StringBuffer();
        for (int i=0; i<n; i++) {
            ith.append(validChars.charAt(0+(int)(Math.random()*62)));
        }
        return ith.toString();
    }
}

//Рабочий метод проверки черновиков и отправленные

/*  String Folder_draft = "/html/body/div[7]/div[3]/div/div[2]/div[1]/div[1]/div[1]/div[2]/div/div/div/div[2]/div/div[1]/div[1]/div/div[4]/div/div/div[2]/span/a";
    String xpath_draft = "/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[2]/div[3]/div[1]/div/table/tbody";
    String Folder_send =  "/html/body/div[7]/div[3]/div/div[2]/div[1]/div[1]/div[1]/div[2]/div/div/div/div[2]/div/div[1]/div[1]/div/div[3]/div/div/div[2]/span/a";
    String xpath_send = "/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[3]/div[3]/div[1]/div/table/tbody";

    public boolean CheckSendDraft (String SendTo, String Subj, String Body, String folder, String xpath, boolean x1) {
        //Проверяем в черновиках
        //Ожидаем
        wait.until(visibilityOfElementLocated(By.xpath(folder)));
        //Находим как WebElement
        WebElement Draft = driver.findElement(By.xpath(folder));
        //Нажимаем на нее,
        Draft.click();

        //Проверяем наличие письма в черновиках
        //Ожидаем
        wait.until(visibilityOfElementLocated(By.xpath(xpath)));
        //Получаем список элементов черновиков писем
        WebElement Table1 = driver.findElement(By.xpath(xpath));
        List<WebElement> Table = Table1.findElements(By.xpath("./tr"));
        boolean x3=false;  //
        //Идем по нему и проверяем сначала совпадение кусочка темы и кусочка тела
        for (WebElement x2: Table ) {
            WebElement l1= x2.findElement(By.xpath("./td[6]/div/div/div/span[1]")); // driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[3]/div[3]/div[1]/div/table/tbody/tr/td[6]/"));
            if (l1.getText().contains(Subj.substring(0,5))) {
                WebElement l2 = x2.findElement(By.xpath("./td[6]/div/div/div/span[2]")); //driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[3]/div[3]/div[1]/div/table/tbody/tr/td[6]/div/div/div/span[2]"));
                if (l2.getText().contains(Body.substring(0,5))) {
                    //Кусок темы и тела есть, заходим в письмо, проверяем остальное
                    l2.click();
                    //Смотрим открытое сообщние
                    List<WebElement> xx = driver.findElements(By.xpath("/html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]"));
                    if (!xx.isEmpty()) { //Сообщение открылось в окне
                        WebElement send= xx.get(0).findElement(By.xpath("./form/div[2]/div/span"));
                        WebElement subj= xx.get(0).findElement(By.xpath("./form/input[@name='subject']")); //
                        WebElement body= xx.get(0).findElement(By.xpath("./table/tbody/tr[1]/td/div/div[1]/div[2]/div[1]/div/table/tbody/tr/td[2]/div[2]/div"));
                        WebElement sendbtn = xx.get(0).findElement(By.xpath("./table/tbody/tr[2]/td/div/div/div[4]/table/tbody/tr/td[1]/div/div[2]"));
                        if ((SendTo.equalsIgnoreCase(send.getText()))&&(Subj.equalsIgnoreCase(subj.getAttribute("value")))&&(Body.equalsIgnoreCase(body.getText()))) {
                            //Отправляем письмо
                            sendbtn.click();
                            x3=true;
                            System.out.println("Проверка успешно завершена, письмо отправлено");
                            break;
                        } else {
                        //Зактрываем это окно
                        WebElement closebtn = driver.findElement(By.xpath("/html/body/div[14]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[2]/div/div[2]/div/div/div/div/table/tbody/tr/td[2]/img[3]"));
                        closebtn.click();
                        }
                    } else { //Если открылось в главном окне
                        //Ожидание
                        wait.until(visibilityOfElementLocated(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[3]/div/table/tr/td[1]/div[2]/div[2]/div/div[3]/div/div/div/div/div/div[1]/div[2]/div[5]/div/div[1]")));
                        //
                        WebElement body= driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[3]/div/table/tr/td[1]/div[2]/div[2]/div/div[3]/div/div/div/div/div/div[1]/div[2]/div[5]/div/div[1]"));
                        //
                        WebElement send= driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[3]/div/table/tr/td[1]/div[2]/div[2]/div[1]/div[3]/div/div/div/div/div/div[1]/div[2]/div[1]/table/tbody/tr[2]/td/table/tbody/tr/td/div[1]/span/span"));
                        //
                        WebElement subj= driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[3]/div/table/tr/td[1]/div[2]/div[1]/div[2]/div[1]/h2"));
                        //
                        WebElement sendbtn = driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[3]/div/table/tr/td[1]/div[2]/div[2]/div[1]/div[3]/div/div/div/div/div/div[2]/div/div/div/div[4]/table/tbody/tr/td[2]/table/tbody/tr[2]/td/div[2]/div[1]/div[4]/table/tbody/tr/td[1]/div/div[2]"));

                        if ((SendTo.equalsIgnoreCase(send.getAttribute("email")))&&(Subj.equalsIgnoreCase(subj.getText()))&&(Body.equalsIgnoreCase(body.getText()))) {
                            //Отправляем письмо
                            sendbtn.click();
                            x3=true;
                            System.out.println("Проверка успешно завершена, письмо отправлено");

                            break;
                        } else {
                            //Это не то письмо, возвращаемся на уровень выше
                            WebElement closebtn = driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[1]/div[3]/div[1]/div/div[1]/div"));
                            closebtn.click();
                        }
                    }
                }
            }
        } return x3;
    }
*/