package All;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;


import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class DriverProvider {
    DriverProvider instance;
    //private WebDriver driver;
    private static Map<Browser, WebDriver> driverMap = new HashMap<Browser, WebDriver>();
    private DriverProvider() { }

    private static Logger log = Logger.getLogger(DriverProvider.class);

    public static WebDriver getDriver(Browser browser, String Path) {
        WebDriver driver = driverMap.get(browser);
        if(driver == null)
        {
            switch(browser)
            {
                case IE:
                    System.setProperty("webdriver.ie.driver", Path);
                    driver = new InternetExplorerDriver();
                    log.debug("Get IE Driver");
                    break;

                case FIREFOX:
                    System.setProperty("webdriver.gecko.driver", Path);
                    driver = new FirefoxDriver();
                    log.debug("Get FireFox Driver");
                    break;

                case CHROME:
                    System.setProperty("webdriver.chrome.driver", Path);
                    driver = new ChromeDriver();
                    log.debug("Get Chrome Driver");
                    break;
            }
            driverMap.put(browser, driver);
        }
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        log.debug("Driver Init");
        return driver;

    }

    public static void quitDriver(Browser browser){
        driverMap.get(browser).quit();
        log.debug("Driver for "+browser+" quit");
        driverMap.put(browser,null);
    }

}
