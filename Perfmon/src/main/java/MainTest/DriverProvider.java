package MainTest;

import org.apache.log4j.Logger;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;
import org.openqa.selenium.winium.WiniumDriverService;

import java.net.URL;
import java.util.concurrent.TimeUnit;

// Чисто как Singleton
public class DriverProvider {

    private static WiniumDriver driver;

    private DriverProvider() { }

    private static Logger log = Logger.getLogger(DriverProvider.class);

    public static WiniumDriver getDriver(WiniumDriverService service, DesktopOptions option) {

        if(driver == null)
        {
            driver = new WiniumDriver(service, option);
        }
        log.debug("Driver Init by Service");
        return driver;

    }

    public static WiniumDriver getDriver (URL service, DesktopOptions option){ //

        if(driver == null)
        {
            driver = new WiniumDriver(service, option);
        }
        log.debug("Driver Init by App");
        return driver;

    }

    public static void closeDriver(){
        log.debug("Winium Driver quit");
        driver = null;
//      driver.close();
    }

}
