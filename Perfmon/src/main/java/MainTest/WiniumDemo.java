package MainTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

//Просто демо
public class WiniumDemo {
    public static void main(String[] args) throws MalformedURLException {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("app","D:\\calc.exe");
//        cap.setCapability("launchDelay","5");
        WebDriver driver = new RemoteWebDriver(new URL("http://localhost:9999"),cap);

        WebElement window = driver.findElement(By.className("CalcFrame"));

        window.findElement(By.id("132")).click();//2
        window.findElement(By.id("93")).click();//+
        window.findElement(By.id("134")).click();//4
        window.findElement(By.id("92")).click();//*
        window.findElement(By.id("134")).click();//4
        window.findElement(By.id("121")).click();//=

        driver.close();
    }
}