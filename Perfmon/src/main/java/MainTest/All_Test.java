package MainTest;

import ProgramWindows.*;
import org.apache.log4j.Logger;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;
import org.openqa.selenium.winium.WiniumDriverService;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

public class All_Test {

    //Режим запуска 1 - старт с сервиса автоматически (от пользователя), 0 - запуск драйвера вручную (с правами админа),
    // т.к. часто сваливается ошибка "Нет доступа" при кликах на элементы, то нужно запускать драйвер отдельно вручную с правами админа (от админа)
    // сейчас нормально работает только с запуском драйвера вручную от админа
    int mode = 0;
    //Режим работы 0 - присоединение к уже открытому и работающему приложению, 1 - запуск приложения с начала
    int work = 1;

    DesktopOptions option;
    WiniumDriver driver;
    WiniumDriverService service;
    public static String WiniumURL = "http://localhost:9999";
    public static String WiniumPath = "C:\\Driver\\Winium.Desktop.Driver.exe";
    public static int port = 9999;
    //Прямые пути
    public static String appPath = "C:\\ProgramWindows\\System32\\perfmon.exe"; //
    public static String appmmc = "C:\\ProgramWindows\\System32\\mmc.exe";
    public static String apparg = "perfmon.msc";
    //Используемые пути
    public static String appsys = System.getenv("windir")+"\\system32\\perfmon.msc";
    public static String appargsys = "/s";
    public static String app = "mmc perfmon.msc";

    //Сложный запуск драйвера через cmd
    public static String user =  ""; //Для запуска от админа - Administrator
    public static String passw = "";//Для запуска от админа - его пароль
    public static String winiumapp = "C:\\Driver\\Winium.Desktop.Driver.exe";
    public static String [] startadm = {"cmd.exe", "/c", "runas /profile /env /user:"+user+" \""+winiumapp+"\"", passw};
    //Простой запуск драйвера через cmd
    public static String [] startapp = {"cmd.exe", "/c", winiumapp};

    //Запуск скрипта мавена для очистки перед и генерации отчета потом
    //Если нужна очистка результатов, используйте комаманду mvn clean перед стартом тестов
    public static String [] cleanmvn = {"cmd.exe", "/c", "mvn clean"};
    public static String [] buildmvn = {"cmd.exe", "/c", "mvn site jetty:run"}; //
    public static String [] startjetty = {"cmd.exe", "/c", "mvn jetty:run"};
    public static String [] startreport = {"cmd.exe", "/c", "start http:\\localhost:8080"};
    //Чтобы это работало, мавен должен быть установлен в системе
    /*
    Установка*
Зайдите на официальный сайт мавен в раздел загрузка и скачайте последнюю стабильную версию.
Распакуйте архив в инсталляционную директорию. Например в C:\Program Files\maven\ в ProgramWindows или /opt/maven в Linux
Установите переменную окружения M2_HOME:
В ProgramWindows кликните правой кнопкой мыши на "мой компьютер" ->свойства->дополнительные параметры->переменные среды->системные переменные и там добавьте "M2_HOME" и "C:\Program Files\maven\" .
В Linux можно добавить строку "export M2_HOME=/opt/maven"в файл /etc/profile .
Установите переменную окружения PATH В ProgramWindows в переменной PATH добавьте к списку директорий строку %M2_HOME%\bin". В Linux можно добавить строку "export PATH=$PATH:$M2_HOME/bin"в файл /etc/profile .
Проверьте корректность установки, набрав в командной строке
mvn -version
Если результат примерно такой
    dima@myhost ~ $ mvn -version
    Apache Maven 3.0 (r1004208; 2010-10-04 15:50:56+0400)
    Java version: 1.6.0_22
    Java home: /opt/sun-jdk-1.6.0.22/jre
    Default locale: ru_RU, platform encoding: UTF-8
    OS name: "linux" version: "2.6.34-gentoo-r12" arch: "amd64" Family: "unix"

то поздравляю, вы успешно установили Maven.
*Во многих дистрибутивах Linux, maven устанавливается автоматически, с помощью менеджера пакетов.

Если что-то не работает
Проверьте, установлен ли у вас JDK.
Для этого наберите в консоли «java -version» ответ должен быть примерно таким:
    java version "1.6.0_22"
    OpenJDK Runtime Environment (IcedTea6 1.10.5) (ArchLinux-6.b22_1.10.5-1-x86_64)
    OpenJDK 64-Bit Server VM (build 19.0-b09, mixed mode)

Проверьте установлена ли переменная окружения JAVA_HOME
Если у вас ProgramWindows наберите в консоли:
echo %JAVA_HOME%

Если у вас Linux наберите в консоли:
echo $JAVA_HOME

команда должна вывести путь к JDK.
     */

    //Путь к папке со скринами
    public static String screenpath = "target/Screenshorts/";
    //Пути к папке со результутами
    public static String allureresultpath = "target/allure-results";
    public static String allurereportpath1= "target/site";
    public static String allurereportpath2= "target/tmp";

    //Объявляем логгер событий
    private static final Logger log = Logger.getLogger(All_Test.class);

    //Комманды сторонним приложениям
    public void startApps (String [] comm) {
        try {
            ProcessBuilder pr = new ProcessBuilder(comm);
            Process process = pr.start();
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            StringBuffer result = new StringBuffer();
            while ((line = br.readLine()) != null) {
                result.append(line);
                log.debug(line);
                //System.out.println(line);
            }
            if (result.toString().contains("BUILD SUCCESS")) {
                log.debug("Maven Work Succsessful");
            }
            if (result.toString().contains("BUILD FAILURE")) {
                log.error("Maven Work NOT Succsessful");
            }
            //process.destroy();
        } catch (IOException ex) {
            log.error("Ошибка  IOException  запуска драйвера !");
            ex.printStackTrace();
        }
        catch (Exception ex) {
            log.error("Ошибка запуска драйвера !");
            ex.printStackTrace();
        }
    }

    //Удаление каталога и его содержимого (да, в java нельзя удалитьпапку, если она не пустая)
    private void delete(final File file) {
        System.out.println("Удаляем файл: " + file.getAbsolutePath());
        if(file.isDirectory()) {
            String[] files = file.list();
            if((null == files) || (files.length == 0)) {
                file.delete();
            } else {
                for(final String filename: files) {
                    delete(new File(file.getAbsolutePath() + File.separator
                            + filename));
                }
                file.delete();
            }
        } else {
            file.delete();
        }
    }

    @BeforeTest
    public void start() throws IOException, InterruptedException {

        //Для очистки, желательно комаманду mvn clean перед стартом тестов

        //Очистка скринов
        delete (new File (screenpath));
        //Очистка результатов
 //     delete (new File (allureresultpath));
        delete (new File (allurereportpath1));
        delete (new File (allurereportpath2));

        //Опции подключения
        option = new DesktopOptions();
        option.setApplicationPath(appsys);
        option.setArguments(appargsys);
        option.setLaunchDelay(5);
        //Присоединятся к уже открытому и работающему приложению - https://github.com/2gis/Winium.Desktop/wiki/Capabilities
        if (work==0) {
            option.setDebugConnectToRunningApp(true);
        }

        //Установка ожидания по умолчанию
         Window.defsec=3;

        if (mode==1) {
            // Убить процесс драйвера, если завис с предыдушего раза, (работает только если запущен с теми же правами, что и тест)
            Process process = Runtime.getRuntime().exec("taskkill /F /IM Winium.Desktop.Driver.exe");
            process.waitFor();
            process.destroy();
            service = new WiniumDriverService.Builder().usingDriverExecutable(new File(WiniumPath)).usingPort(port).withVerbose(true).withSilent(false).buildDesktopService();
            service.start();
            this.driver = DriverProvider.getDriver(service, option);
        }
        if (mode==0) {
//          startApps(WiniumComm);
            this.driver = DriverProvider.getDriver(new URL (WiniumURL), option);
        }
        //Не реализовано в Winium
//        this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        this.driver.manage().window().maximize();
    }

    @AfterTest
    public void tearDown() throws IOException, InterruptedException {
        MainWindow cv = new MainWindow(driver);
        cv.CloseWnd();
        // Убить процесс драйвера, (работает только если запущен с теми же правами, что и тест)
/*      Process process = Runtime.getRuntime().exec("taskkill /F /IM Winium.Desktop.Driver.exe");
        process.waitFor();
        process.destroy();
*/
        //Обнуляем драйвер
        DriverProvider.closeDriver();

        startApps(startreport);
        startApps(buildmvn);
//      startApps(startjetty);

    }
/**/
    @Test (priority = 1)
    public void openWindow () {
        // Логирование действий
        log.debug(">>>>>  Open Window >>>>\r\n");

        MainWindow mw1 = new MainWindow(driver);
        Assert.assertTrue(mw1.OpenAndMaximize());
    }

    @Test (dependsOnMethods="openWindow", priority = 2)
    public void OpenSysMon  () {
        // Логирование действий
        log.debug(">>>>>  Open SysMon >>>>\r\n");

        MainWindow mw = new MainWindow(driver);
        mw.GetFocus();
        MainWindow mw1 = new MainWindow(driver);
        Assert.assertTrue(mw1.openSysMon());
    }
    /**/
        @Test (dependsOnMethods="OpenSysMon", priority = 3)//
        public void DelCountSysMon  () {
            // Логирование действий
            log.debug(">>>>>  Del Count in SysMon >>>>\r\n");

            MainWindow mw = new MainWindow(driver);
            mw.GetFocus();
            MainWindow mw1 = new MainWindow(driver);
            List<String> sl = Arrays.asList("Сведения о процессоре" , "Процессор");
            Assert.assertTrue(mw1.DelCounter("% загруженности процессора", sl)); //,
        }

    @Test (dependsOnMethods="openWindow", priority = 4)//
    public void AddCountSysMon  () {
        // Логирование действий
        log.debug(">>>>>  Add Count in SysMon >>>>\r\n");

        MainWindow mw = new MainWindow(driver);
        mw.GetFocus();
        MainWindow mw1 = new MainWindow(driver);
        List<String> sl = Arrays.asList("Скорость записи на диск (байт/с)");
        List<String> sc = Arrays.asList("Физический диск");
        Assert.assertTrue((mw1.AddCounters("Физический диск", sl, "_Total"))&&(mw1.CheckCount("Скорость записи на диск (байт/с)",sc ))); //,
    }

}
