package ProgramWindows;

import MainTest.All_Test;
import MainTest.MyAction;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.winium.WiniumDriver;

import java.util.ArrayList;
import java.util.List;

public class AddCountWindow extends Window {

    //Объявляем логгер событий
    private static final Logger log = Logger.getLogger(All_Test.class);


    //Главное окно "Добавить счетчики"
    //@FindBy(xpath ="//*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']") //
    protected WebElement genwnd;

    //Заголовок окна
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='TitleBar']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='TitleBar']")
    protected WebElement titlebar;



    //Выбор счетчиков с компьютера (список для значенией)
    //@FindBys(@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1001']/*[@AutomationId='ListBox']/*[@ControlType='ControlType.ListItem']"))
    @FindBys(@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1001']/*[@AutomationId='ListBox']/*[@ControlType='ControlType.ListItem']"))
    protected List<WebElement> compcountlst;

    //Выбор счетчиков с компьютера - Кнопка раскрытия списка
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1001']/*[@AutomationId='DropDown']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1001']/*[@AutomationId='DropDown']")
    protected WebElement expandbtn;

    //Выбор счетчиков с компьютера - Кнопка "Обзор"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1003']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1003']")
    protected WebElement browsebtn;


    //Список всех основных счетчиков (не развернутых)
    //@FindBys(@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1006']/*[@ControlType='ControlType.Group']"))
    @FindBys(@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1006']/*[@ControlType='ControlType.Group']"))
    protected List<WebElement> countlist;

    //Селектор для получения Подсчетчиков для счетчика
    protected By subcount = By.xpath("./*[@ControlType='ControlType.DataItem']");

    //Общая панель счетчиков
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1006']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1006']")
    protected WebElement countpnl;

    //Вертикальная прокрутка на сетке "Счетчики", кнопка "Вверх"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1006']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='SmallDecrement']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1006']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='SmallDecrement']")
    protected WebElement upcountbtn;

    //Вертикальная прокрутка на сетке "Счетчики", кнопка "СовсемВверх"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1006']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='LargeDecrement']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1006']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='LargeDecrement']")
    protected WebElement uplrgcountbtn;

    //Вертикальная прокрутка на сетке "Счетчики", "Бегунок"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1006']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='Thumb']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1006']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='Thumb']")
    protected WebElement trumpcount;

    //Вертикальная прокрутка на сетке "Счетчики", кнопка "СовсемВниз"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1006']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='LargeIncrement']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1006']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='LargeIncrement']")
    protected WebElement downlrgcountbtn;

    //Вертикальная прокрутка на сетке "Счетчики", кнопка "Вниз"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1006']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='SmallIncrement']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1006']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='SmallIncrement']")
    protected WebElement downcountbtn;



    //Флаг "Отображать описание"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1008']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1008']")
    protected WebElement wievflag;


    //Общая панель "Список экземпляров выбранного объекта" (_Total)
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1011']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1011']")
    protected WebElement objpnl;

    //Список экземпляров выбранного объекта (_Total)
    //@FindBys(@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1011']/*[@ControlType='ControlType.ListItem']"))
    @FindBys(@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1011']/*[@ControlType='ControlType.ListItem']"))
    protected List<WebElement> objlist;

    //Общая Вертикальная прокрутка на списке экземпляров, кнопка "Вверх"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1011']/*[@AutomationId='Vertical ScrollBar']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1011']/*[@AutomationId='Vertical ScrollBar']")
    protected WebElement verticalobjscr;

    //Вертикальная прокрутка на списке экземпляров, кнопка "Вверх"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1011']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='SmallDecrement']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1011']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='SmallDecrement']")
    protected WebElement upobjbtn;

    //Вертикальная прокрутка на списке экземпляров, кнопка "СовсемВверх"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1011']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='LargeDecrement']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1011']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='LargeDecrement']")
    protected WebElement uplrgobjbtn;

    //Вертикальная прокрутка на списке экземпляров, "Бегунок"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1011']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='Thumb']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1011']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='Thumb']")
    protected WebElement trumpobjbtn;

    //Вертикальная прокрутка на списке экземпляров, кнопка "СовсемВниз"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1011']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='LargeIncrement']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1011']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='LargeIncrement']")
    protected WebElement downlrgobjtbtn;

    //Вертикальная прокрутка на списке экземпляров, кнопка "Вниз"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1011']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='SmallIncrement']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1011']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='SmallIncrement']")
    protected WebElement downobjbtn;



    //Список вхождений
    //@FindBys(@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1012']/*[@AutomationId='ListBox']/*[@ControlType='ControlType.ListItem']"))
    @FindBys(@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1012']/*[@AutomationId='ListBox']/*[@ControlType='ControlType.ListItem']"))
    protected List<WebElement> inlist;

    //Кнопка развернуть список
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1012']/*[@AutomationId='DropDown']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1012']/*[@AutomationId='DropDown']")
    protected WebElement inlistbtn;

    //Кнопка "Поиск"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1013']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1013']")
    protected WebElement findbtn;



    //Сетка Добавленные "Счетчики"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1015']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1015']")
    protected WebElement countaddedpnl;

    //Список добавленных счетчиков
    //@FindBys(@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1015']/*[@ControlType='ControlType.Group']"))
    @FindBys(@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1015']/*[@ControlType='ControlType.Group']"))
    protected List<WebElement> addedcountlist;

    //Селектор для получения Подсчетчиков для добавленного счетчика
    protected By subaddedcount = By.xpath("./*[@ControlType='ControlType.DataItem']");

    //Селектор для получения из Подсчетчиков для добавленного счетчика свойства "Экземпляр"
    protected By totalsubaddedcount = By.xpath("./*[3]"); //protected By totalsubaddedcount = By.xpath("./*[@ControlType='ControlType.DataItem']/*[@ControlType='ControlType.DataItem']/*[3]");

    //Вертикальная прокрутка на сетке "Счетчики", кнопка "Вверх"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1015']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='SmallDecrement']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name=]/*[@AutomationId='1015']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='SmallDecrement']")
    protected WebElement upaddedcountbtn;

    //Вертикальная прокрутка на сетке "Счетчики", кнопка "СовсемВверх"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1015']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='LargeDecrement']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1015']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='LargeDecrement']")
    protected WebElement uplrgaddedcountbtn;

    //Вертикальная прокрутка на сетке "Счетчики", "Бегунок"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1015']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='Thumb']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1015']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='Thumb']")
    protected WebElement trumpaddedcountbtn;

    //Вертикальная прокрутка на сетке "Счетчики", кнопка "СовсемВниз"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1015']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='LargeIncrement']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1015']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='LargeIncrement']")
    protected WebElement downlrgaddedcountbtn;

    //Вертикальная прокрутка на сетке "Счетчики", кнопка "Вниз"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1015']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='SmallIncrement']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1015']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='SmallIncrement']")
    protected WebElement downaddedcountbtn;



    //Кнопка "Добавить"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1014']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1014']")
    protected WebElement addbutton;

    //Кнопка "Удалить"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1016']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1016']")
    protected WebElement delbtn;

    //Кнопка "ОК"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='1']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='1']")
    protected WebElement okbutton;

    //Кнопка "Отмена"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='2']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='2']")
    protected WebElement cancelbtn;

    //Кнопка Справка
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='2007']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='2007']")
    protected WebElement helpbtn;

    //Кнопка закрыть на титлбаре
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770' and @Name='Добавить счетчики']/*[@AutomationId='TitleBar']/*[@AutomationId='Close']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='#32770']/*[@AutomationId='TitleBar']/*[@AutomationId='Close']")
    protected WebElement clsbtn;

    //Скрин открыго и первого окна
    protected String openWnd = All_Test.screenpath+"OpenAddCounterWnd_screenshot.png";
    //Скрин открыго окна перед закрытием
    protected String finalWnd = All_Test.screenpath+"BeforeCloseAddWnd_screenshot.png";


    public AddCountWindow (WiniumDriver driver) {
        super(driver);
    }

    public boolean GetFocus () {
        //Не реализовано в winium
//      driver.switchTo().frame(genwnd);
        elementWait(titlebar, 10);
        if (isVisible(titlebar)){
            titlebar.click();
            log.debug("Фокус получен/Focus is");
        } else {
            log.error("Заголовок окна не найден при получении фокуса/TitleBar NOT found to get focus");
        }
        return isVisible(titlebar);
    }

    public boolean exit (boolean key) {

         if ((key)) {
             elementWait(okbutton);
             //Делаем скрин
             doscreen(finalWnd);
             log.debug("Окно добавление счетчиков закрывается сейчас - ОК/AddCounterWindows are closed now - ОК");
             okbutton.click();
         } else {
             elementWait(cancelbtn);
             log.error("Окно добавление счетчиков закрывается сейчас - Отмена/AddCounterWindows are closed now - Cancel");
             cancelbtn.click();
         }
        return !isVisible(genwnd);
    }

    public boolean checkOpen () {

        elementWait(countpnl, 15);
        elementWait(addbutton,15);
        elementWaitEnamble(addbutton,240);
        this.GetFocus();
        //Делаем скрин
        doscreen(openWnd);
        log.debug("Окно добавление счетчиков открыто сейчас/AddCounterWindows are open now ");
        return isVisible(titlebar);
    }


    public boolean selectSubElem ( boolean expand, boolean up, By by, WebElement wgroup, List<String> counters, String obj, WebElement pnl, WebElement upbtn, WebElement uplrgbtn, WebElement dwlrgbtn, WebElement dwbtn, List<WebElement> list, MyAction action ) {
        elementWait(pnl, 7);

        // Если надо проверить всю группу (добавлена целиком)
        if (counters.isEmpty()) {
            log.debug("Группа присутствует и добавлена целиком !/Group add fully !");
            return true;
        }

        //Порядок обхода списка, если false - то снизу
        if (!up) {
            WebElement temp;

            temp = upbtn;
            upbtn = dwbtn;
            dwbtn = temp;

            temp = uplrgbtn;
            uplrgbtn = dwlrgbtn;
            dwlrgbtn = temp;
        }

        //Если группа не развернута
        //Нельзя просто использовать if (wgroup.getAttribute("ExpandCollapse.ExpandCollapseState") .... ) { //Expanded - развернута Collapsed - свернута
        //не находит это свойство, т.к. возвращает оно перечисление
        // public enum ExpandCollapseState {
        //    Collapsed,
        //    Expanded,
        //    PartiallyExpanded,
        //    LeafNode
        //}

        if (expand) { //true - разворачивать надо
            //Кликаем 2 раза для разворачивания
            log.debug("Разворачиваем группу/Expand group");
            Actions action3 = new Actions(driver);
            Action moveup = action3.doubleClick(wgroup).build();
            moveup.perform();
        }

        //Группа развернута
        List<WebElement> xv = wgroup.findElements(by);

        //Если группа не имеет подгруппы, а список счетчиков не пуст - ошибка
        if (xv.isEmpty()) {
            log.debug("Группа не имеет счетчиков/Group dont have counters");
            return false;
        }

        //Формируем список WebElements, удовлетворяющих условиям
        List<WebElement> nlist = new ArrayList<>();
        for (String g: counters){
            for (WebElement x : xv) {
                if (x.getAttribute("Name").equalsIgnoreCase(g)) {
                    nlist.add (x);
                }
            }
        }
        if (counters.size()!= nlist.size()) {
            log.error("Не все указаные счетчики найдены в группе/Not all counters found in group");
            return false;
        }

        //Массив для проверки все ли счетчики добавлены, инициализиров. false
        boolean x[] = new boolean [nlist.size()];
        for(int j=0; j<x.length; j++){
            x[j]=false;
        }

        boolean g = false;
        while (!g){

            //Проходим по счетчикам группы
            for(int t=0; t<nlist.size(); t++) { //Идем по тем счетчикам, что нам нужны
                //Если счетчик виден и подходит под условия, то
                if ((isVisible(nlist.get(t)))&&(nlist.get(t).getAttribute("IsOffscreen").equalsIgnoreCase("False"))&&(!(nlist.get(t).getAttribute("ClickablePoint").equalsIgnoreCase("(null)")))) {
                    action.action(nlist, t, x);
                }
            }

            //Проверяем все
            g=true;
            for(int t=0; t<x.length; t++) {
                if (!x[t]) {
                    g = false;
                }
            }

            if ((g)||(isVisible(dwlrgbtn))&&((dwlrgbtn.getAttribute("ClickablePoint").equalsIgnoreCase("(null)")))&&(dwlrgbtn.getAttribute("IsOffscreen").equalsIgnoreCase("True"))) {
                break;
            }
            if (isVisible(dwbtn)) {
                dwbtn.click();
            }

        } return g;
    }


    public boolean checkCounters (String group, List<String> counters, String obj ) {

        WebElement wgroup = findInList(3, true, group, countaddedpnl, upaddedcountbtn, uplrgaddedcountbtn, downlrgaddedcountbtn, downaddedcountbtn, addedcountlist);
        if (wgroup==null) {
            log.error("Группа не найдена в списке при проверке !/Group noe found in checking !");
            return false;
        }

        boolean check = selectSubElem(false,true, subaddedcount, wgroup, counters, obj, countaddedpnl, upaddedcountbtn, uplrgaddedcountbtn, downlrgaddedcountbtn, downaddedcountbtn, addedcountlist, new MyAction() {
            @Override
            public void action(List<WebElement> xy, int index, boolean x[]) {
                WebElement xx = xy.get(index).findElement(totalsubaddedcount);
                if ((xx.getAttribute("Name").equalsIgnoreCase(obj))) { // if ((isVisible(xx))&&(xx.getAttribute("Name").equalsIgnoreCase(obj))) {
                    x[index] = true;
                    xy.get(index).click();
                    xx.click();
                }
            }
        });
        if (!check) {
            log.error("Не все счетчики добавлены при проверке !/Not all counters added in checking !");
            return false;
        }
             return check;
        }

    //Добавляет группу счетчики в наблюдение, если надо всю группу - пустой список счетчиков
    public boolean AddCounters (String group, List<String> counters, String obj ) { //, List<String> Obj

        WebElement wgroup = findInList (3,false, group, countpnl, upcountbtn, uplrgcountbtn, downlrgcountbtn, downcountbtn, countlist );
        if (wgroup==null) {
            log.error("Группа не найдена в списке при добавлении !/Group noe found in added !");
            return false;
        }
        //Добавление всей целиком
        if (counters.isEmpty()) {
            WebElement y = findInList(3,true, obj, objpnl, upobjbtn, uplrgobjbtn, downlrgobjtbtn, downobjbtn, objlist);
            if (y==null) {
                log.error("Не найдено \""+obj+"\" экземпляра объета в списке при установке счетчика"+group+"/Not found \"+obj+\" object in list in set counter"+group);
                return false;
            } else {
                addbutton.click();
                return checkCounters (group, counters, obj);
            }
        }

        boolean add = selectSubElem(true,true, subcount, wgroup, counters, obj, countpnl, upcountbtn, uplrgcountbtn, downlrgcountbtn, downcountbtn, countlist, new MyAction() {
            @Override
            public void action (List<WebElement> xy, int index, boolean[] x) {
                xy.get(index).click();
                WebElement y = findInList(3,true, obj, objpnl, upobjbtn, uplrgobjbtn, downlrgobjtbtn, downobjbtn, objlist);
                if (y==null) {
                    log.error("Не найдено \""+obj+"\" экземпляра объета в списке при установке счетчика"+xy.get(index).getAttribute("Name")+"/Not found \"+obj+\" object in list in set counter"+xy.get(index).getAttribute("Name"));
                } else {
                    x[index] = true;
                    addbutton.click();
                }
            }
        });
        if (!add) {
            log.error("Не все счетчики добавлены !/Not all counters added !");
            return false;
        }

    return checkCounters (group, counters, obj);
    }

}