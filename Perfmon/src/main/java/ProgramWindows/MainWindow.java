package ProgramWindows;

import MainTest.All_Test;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.winium.WiniumDriver;

import java.util.List;

public class MainWindow extends Window {

    //Объявляем логгер событий
    private static final Logger log = Logger.getLogger(All_Test.class);

    //Главное окно perfmon - есть всегда
    @FindBy(className ="MMCMainFrame")
    protected WebElement genwnd;

    //Меню perfmon - есть всегда
    @FindBy(id ="59648") //@FindBy(className ="MDIClient")
    protected WebElement genpnl;

    //Рабочая панель perfmon - есть всегда
    @FindBy(id ="13341") //@FindBy(className ="SizeableRebar")
    protected WebElement menupnl;

    //Заголовок окна - есть всегда
    @FindBy(id ="TitleBar")
    protected WebElement titlebar;

    //Рабочее окно "Производительность" perfmon
    @FindBy(className ="MMCViewWindow")
    protected WebElement genperfwnd;

    //Рабочая древовидная панель "Производительность" с выбором средств наблюдения perfmon
    @FindBy(id ="12785")
    protected WebElement gentreepnl;

    //Элемент древовидной панели "Производительность"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']//*[@AutomationId='12785']/*[@ControlType='ControlType.TreeItem' and @Name='Производительность']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='MDIClient']/*[@ClassName='MMCChildFrm']/*[@ClassName='MMCViewWindow']/*[@AutomationId='12785']/*[@ControlType='ControlType.TreeItem']")
    protected WebElement perfelem;

    //Элемент древовидной панели "Производительность->Средства наблюдения"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']//*[@AutomationId='12785']/*[@ControlType='ControlType.TreeItem' and @Name='Производительность']/*[@ControlType='ControlType.TreeItem' and @Name='Средства наблюдения']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='MDIClient']/*[@ClassName='MMCChildFrm']/*[@ClassName='MMCViewWindow']/*[@AutomationId='12785']/*[@ControlType='ControlType.TreeItem']/*[@ControlType='ControlType.TreeItem']")
    protected WebElement watch;

    //Элемент древовидной панели "Производительность->Средства наблюдения->Системный монитор"
    //@FindBy(xpath ="*[@ClassName='MMCMainFrame']//*[@AutomationId='12785']/*[@ControlType='ControlType.TreeItem' and @Name='Производительность']/*[@ControlType='ControlType.TreeItem' and @Name='Средства наблюдения']/*[@ControlType='ControlType.TreeItem' and @Name='Системный монитор']")
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='MDIClient']/*[@ClassName='MMCChildFrm']/*[@ClassName='MMCViewWindow']/*[@AutomationId='12785']/*[@ControlType='ControlType.TreeItem']/*[@ControlType='ControlType.TreeItem']/*[@ControlType='ControlType.TreeItem']")
    protected WebElement sysmon;

    //Панель инструментов SysMon
    @FindBy(id ="20")
    protected WebElement syspnl;

    //Панель графиков SysMon
    // Разный AutomationId на разных версиях ProgramWindows 7: @FindBy(id ="3") на коорпоративная и @FindBy(id ="75202800") на домашняя базовая
//  @FindBy(id ="3")
//  protected WebElement grphpnl;

    //Кнопка "Добавить" на панели инструментов SysMon
    @FindBy(id ="Item 24")
    protected WebElement addcountbtn;

    //Кнопка "Удалить" на панели инструментов SysMon
    @FindBy(id ="Item 25")
    protected WebElement delcountbtn;

    //Таблица со счетчиками у SysMon внизу
    @FindBy(id ="100")
    protected WebElement counttbl;

    //Кнопка прокрутки вниз у таблицы со счетчиками у SysMon внизу
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='MDIClient']/*[@ClassName='MMCChildFrm']/*[@ClassName='MMCViewWindow']//*[@AutomationId='100']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='SmallIncrement']")
    protected WebElement downbtn;

    //Кнопка прокрутки совсем вниз на скролле у таблицы со счетчиками у SysMon внизу
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='MDIClient']/*[@ClassName='MMCChildFrm']/*[@ClassName='MMCViewWindow']//*[@AutomationId='100']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='LargeIncrement']")
    protected WebElement downlrgbtn;

    //Кнопка прокрутки совсем вверх на скролле у таблицы со счетчиками у SysMon внизу
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='MDIClient']/*[@ClassName='MMCChildFrm']/*[@ClassName='MMCViewWindow']//*[@AutomationId='100']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='LargeDecrement']")
    protected WebElement uplrgbtn;

    //Кнопка прокрутки вверх у таблицы со счетчиками у SysMon внизу
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='MDIClient']/*[@ClassName='MMCChildFrm']/*[@ClassName='MMCViewWindow']//*[@AutomationId='100']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='SmallDecrement']")
    protected WebElement upbtn;

    //Вертикальная полоса прокрутки у таблицы со счетчиками у SysMon внизу
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='MDIClient']/*[@ClassName='MMCChildFrm']/*[@ClassName='MMCViewWindow']//*[@AutomationId='100']/*[@AutomationId='Vertical ScrollBar']")
    protected WebElement verticalbar;

    //Бегунок вертикальной прокрутки у таблицы со счетчиками у SysMon внизу
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@ClassName='MDIClient']/*[@ClassName='MMCChildFrm']/*[@ClassName='MMCViewWindow']//*[@AutomationId='100']/*[@AutomationId='Vertical ScrollBar']/*[@AutomationId='Thumb']")
    protected WebElement verticalthumph;

    //Список строчек со счетчиками со счетчиками у SysMon внизу
    @FindBys(@FindBy(xpath="*[@ClassName='MMCMainFrame']/*[@ClassName='MDIClient']/*[@ClassName='MMCChildFrm']/*[@ClassName='MMCViewWindow']//*[@AutomationId='100']/*[@ControlType='ControlType.DataItem' and @Name='Show']")) //@FindBy(id ="100")
    protected List<WebElement> listcountstr;

    //Список полей "Счетчик" в строчках со счетчиками со счетчиками у SysMon внизу
    @FindBys(@FindBy(xpath="*[@ClassName='MMCMainFrame']/*[@ClassName='MDIClient']/*[@ClassName='MMCChildFrm']/*[@ClassName='MMCViewWindow']//*[@AutomationId='100']/*[@ControlType='ControlType.DataItem' and @Name='Show']/*[5]")) //@FindBy(id ="100") and @LocalizedControlType='элемент'
    protected List<WebElement> listcountfield;

    //Список полей "Объект" в строчках со счетчиками со счетчиками у SysMon внизу
    @FindBys(@FindBy(xpath="*[@ClassName='MMCMainFrame']/*[@ClassName='MDIClient']/*[@ClassName='MMCChildFrm']/*[@ClassName='MMCViewWindow']//*[@AutomationId='100']/*[@ControlType='ControlType.DataItem' and @Name='Show']/*[8]")) //@FindBy(id ="100") and @LocalizedControlType='элемент'
    protected List<WebElement> listobjfield;

    //Кнопка развернуть
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@AutomationId='TitleBar']/*[@AutomationId='Maximize' and @ControlType='ControlType.Button']") //@FindBy(xpath ="/[@AutomationId='TitleBar']/[@AutomationId='Maximize']")  [@ClassName='MMCMainFrame']   @FindBy(xpath ="/MMCMainFrame/*/[@AutomationId='Maximize']")
    protected WebElement maximizebtn;

    //Кнопка восстановить маленькое окно
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@AutomationId='TitleBar']/*[@AutomationId='Restore' and @ControlType='ControlType.Button']") //@FindBy(xpath ="/[@AutomationId='TitleBar']/[@AutomationId='Maximize']")  [@ClassName='MMCMainFrame']   @FindBy(xpath ="/MMCMainFrame/*/[@AutomationId='Maximize']")
    protected WebElement restorebtn;

    //Кнопка закрыть
    @FindBy(xpath ="*[@ClassName='MMCMainFrame']/*[@AutomationId='TitleBar']/*[@AutomationId='Close' and @ControlType='ControlType.Button']") //@FindBy(xpath ="/[@AutomationId='TitleBar']/[@AutomationId='Maximize']")  [@ClassName='MMCMainFrame']   @FindBy(xpath ="/MMCMainFrame/*/[@AutomationId='Maximize']")
    protected WebElement clsbtn;

    //Меню файл
    @FindBy(id ="Item 21513") //@FindBy(xpath ="/MMCMainFrame//[@AutomationId='Item 21513']") @FindBy(xpath ="/[@AutomationId='13341']/[@AutomationId='4096']/[@AutomationId='4097']/[@AutomationId='Item 21513']")
    protected WebElement filebmn;

    //Скрин открыго и развернутого (или нет) окна
    protected String openWnd = All_Test.screenpath+"OpenAndMaximize_screenshot.png";
    //Скрин открыго Системного монитора
    protected String openSysMon = All_Test.screenpath+"OpenSysMon_screenshot.png";
    //Скрин удаленного счетчика
    protected String delCounter = All_Test.screenpath+"DelCounterWnd_screenshot.png";
    //Скрин выделенного счетчика
    protected String selCounter = All_Test.screenpath+"SelCounterWnd_screenshot.png";
    //Скрин окна перед закрытием
    protected String finalWnd = All_Test.screenpath+"BeforeCloseGenWnd_screenshot.png";

    public MainWindow (WiniumDriver driver) {
        super(driver);
    }

    public boolean GetFocus () {
        //Не реализовано в winium
//      driver.switchTo().frame(genwnd);
        if (isVisible(titlebar)){
            titlebar.click();
            log.debug("Фокус получен/Focus is");
        } else {
            log.error("Заголовок окна не найден/TitleBar NOT found");
        }
        return isVisible(genwnd);
    }

    public boolean AddCounters (String counter, List<String> counters, String obj) {
        if(isVisible(addcountbtn)) {
            addcountbtn.click();
            AddCountWindow xx = new AddCountWindow(driver);
            if (xx.checkOpen()) { //Окно нормально открылось
                if (xx.AddCounters( counter,  counters,  obj)) {
                    log.debug("Счетчик успешно добавлен/Counter add Successful");
                    xx.exit(true);
                    return true;
                } else {
                    log.error("Окно добавления счетчика не открыто/AddCounter Window NOT opened");
                    return false;
                }
            }
            }
            log.error("Кнопка добавления счетчика не видна/Button to add Counter NOR visible");
            return false;
    }

    public boolean OpenAndMaximize ()  {
        //Не реализовано в winium
//      driver.switchTo().frame(genwnd);
        elementWait(titlebar,3);
        titlebar.click();
       if(isVisible(maximizebtn)){
            maximizebtn.click();
            log.debug("Окно развернуто/Window are maximize ");
       } else {
            log.debug("Окно уже было развернуто/Window are maximize before");
       }
        //Делаем скрин
        doscreen(openWnd);
        return ((isVisible(genwnd))&&(isVisible(titlebar)));
    }

    public boolean openSysMon () {

        elementWait(perfelem,10);

        if (!isVisible(sysmon)) { //Если доступен сразу

            //Кликаем на производительность, и если надо разворачиваем
            perfelem.click();

            //Может быть развернуто, а может и нет, поэтому так
            if (!isVisible(watch)) {
                Actions action = new Actions(driver);
                Action dblclick = action.doubleClick(perfelem).build();
                dblclick.perform();
                elementWait(watch, 5);
                log.debug("Элемент \"Производительность\" уже развернут/\"Performance\" element are expand before");
            }

            MainWindow x1 = new MainWindow(driver);
            x1.watch.click();

            if (!isVisible(sysmon)) {
                Actions action1 = new Actions(driver);
                Action dblclick1 = action1.doubleClick(x1.watch).build();
                dblclick1.perform();
                elementWait(sysmon, 5);
                log.debug("Элемент \"Средства наблюдения\" уже развернут/\"Watch\" element are expand before");
            }

        }
        MainWindow x2 = new MainWindow(driver);
        x2.sysmon.click();

        if (!isVisible(syspnl)) {
            Actions action2 = new Actions(driver);
            Action dblclick2 = action2.doubleClick(x2.sysmon).build();
            dblclick2.perform();
            elementWait(syspnl,5);
            log.debug("Элемент \"Системный монитор\" уже развернут/\"SystemMonitor\" element are opened before");
        }
        //Делаем скрин
        doscreen(openSysMon);
        return (  (isVisible(syspnl)) && (isVisible(counttbl))  );
    }

    //Проверка, есть или нет элемент в списке счетчиков, если есть, он выделяется нажатием
    public boolean DelCounter (String counter, List<String> Obj) { //, String Obj
        elementWait(counttbl);
            if (CheckCount(counter, Obj)) { //Если это тот элемент, и он есть и выделен, удаляем этот счетчик , Obj
                //Делаем скрин
                doscreen(selCounter);
                MainWindow mw = new MainWindow(driver);
                elementWait(delcountbtn);
                if(isEnable(delcountbtn)){
                    mw.delcountbtn.click();
                    log.debug("Cчетчик "+counter+" удален (нажата кнопка)/"+counter+" counter are deleted (button pressed)");
                    //Делаем скрин
                    doscreen(delCounter);
                }
            } else {
                log.error("Cчетчик \"+counter+\" не найден для удаления/"+counter+" counter not found for deleted");
                return true;
            }
        MainWindow mw = new MainWindow(driver);
        return !mw.CheckCount(counter, Obj); //, Obj
    }

    //Проверка, есть или нет элемент в списке счетчиков, если есть, он выделяется нажатием
    public boolean CheckCount (String group, List<String> Obj) { //

        WebElement wgroup = findInList(3,true, group, counttbl, upbtn, uplrgbtn, downlrgbtn, downbtn, listcountfield);
        if (wgroup==null) {
            return false;
        } else {
            for (String x: Obj){
                if (listobjfield.get(listcountfield.indexOf(wgroup)).getAttribute("Name").equalsIgnoreCase(x)) {
                    log.debug("Cчетчик "+group+" найден при поиске /"+group+" counter are found in find");
                    listcountfield.get(listcountfield.indexOf(wgroup)).click();
                    wgroup.click();
                    return true;
                }
            }
            log.error("Cчетчик "+group+" не найден при поиске/"+group+" counter not found in find");
            return false;
        }
    }

    public boolean CloseWnd () {
        MainWindow x2 = new MainWindow(driver);
        elementWait(clsbtn);
        x2.GetFocus();
//      x2.filebmn.click();
        //Делаем скрин
        doscreen(finalWnd);
        log.debug("Главное окно закрывается сейчас/Main ProgramWindows are closed now");
        clsbtn.click();
        return isVisible(titlebar);
    }
}
