package ProgramWindows;

import MainTest.All_Test;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.winium.WiniumDriver;

import java.io.File;
import java.util.List;

public class Window {

    protected WiniumDriver driver;
    private WebElement webElement;

    //Объявляем логгер событий
    private static final Logger log = Logger.getLogger(All_Test.class);

    //Ожидание по умолчанию
    public static long defsec = 3;

    public Window(WiniumDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    protected void elementWait (WebElement webElement, long sec){
        if (sec<0) {sec*=-1;}
        WebDriverWait wait = new WebDriverWait(driver, sec);
        wait.until(ExpectedConditions.visibilityOf(webElement));
        //WebElement el = new WebDriverWait(driver, 5).until(ExpectedConditions.elementToBeClickable(webElement));
    }

    protected void elementWait (WebElement webElement){
        if (defsec<0) {defsec*=-1;}
        WebDriverWait wait = new WebDriverWait(driver, defsec);
        wait.until(ExpectedConditions.visibilityOf(webElement));
        //WebElement el = new WebDriverWait(driver, 5).until(ExpectedConditions.elementToBeClickable(webElement));
    }

    protected void elementWaitEnamble (WebElement webElement, long sec){
        if (defsec<0) {defsec*=-1;}
        WebDriverWait wait = new WebDriverWait(driver, sec);
        wait.until( new ExpectedCondition<Boolean> (){
            @Override
            public Boolean apply(WebDriver driver) {
               return isEnable(webElement);
            }
        });
        //WebElement el = new WebDriverWait(driver, 5).until(ExpectedConditions.elementToBeClickable(webElement));
    }

    protected boolean isEnable (WebElement webElement) {
        try{
            if(webElement.isEnabled()){
                return true;
            } else {
                return false;
            }
        }catch (Exception ex) {
            return false;
        }
    }

    protected boolean isVisible(WebElement webElement) {
        try{
            if(webElement.isDisplayed()){
                return true;
            } else {
                return false;
            }
        }catch (Exception ex) {
            return false;
        }
    }

    public void doscreen (String path) {
        try {
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            // Now you can do whatever you need to do with it, for example copy somewhere
            FileUtils.copyFile(scrFile, new File(path)); //"c:\\tmp\\screenshot.png"
        } catch (Exception ex) {
            log.debug("Не удалось сделать скриншот");
        }
    }

    public WebElement findInList (int n, boolean up, String element, WebElement pnl, WebElement upbtn, WebElement uplrgbtn, WebElement dwlrgbtn, WebElement dwbtn, List<WebElement> list) {
        elementWait(pnl, 7);
        WebElement wgroup = null;
        //Находим нужную группу в списке по ее названию (и проверяем есть ли вообще она)
        for (int i=0; i<list.size(); i++) {
            if (list.get(i).getAttribute("Name").equalsIgnoreCase(element)) {
                wgroup = list.get(i);
                break;
            }
        }
        //Если такой группы вообще нет - это ошибка
        if (wgroup == null) {
            log.error("Элемент \""+element+"\" не найден в списке ");
            return null;
        }

        //Если элемент не виден сейчас - будем искать
        if (!isVisible(wgroup)) {
            //Если не видим и нет скролла - ошибка
            if (!isVisible(upbtn)) {
                log.error("Отсутствие скролла при невидимости элемента \""+element);
                return null;
            }
            Actions action2 = new Actions(driver);

            //Порядок обхода списка, если false - то снизу
            if (!up) {
                WebElement temp;

                temp = upbtn;
                upbtn = dwbtn;
                dwbtn = temp;

                temp = uplrgbtn;
                uplrgbtn = dwlrgbtn;
                dwlrgbtn = temp;
            }

            //Да, такая прокрутка, ибо в winium для java не реализована функция MouseDown и просто переместить ее с бегунком нельзя.
            // org.openqa.selenium.UnsupportedCommandException: 'getElementLocation' is not valid or implemented command. (WARNING: The server did not provide any stacktrace information)
            // org.openqa.selenium.UnsupportedCommandException: 'mouseDown' is not valid or implemented command. (WARNING: The server did not provide any stacktrace information)
            /*
                //Если есть бегунок
                if(isVisible(verticalthumph)) {
                    verticalthumph.click();
                    Action moveup = action2.clickAndHold(verticalthumph).moveByOffset(10, 10).release().build(); //upbtn.getLocation().x  upbtn.getLocation().y
                    moveup.perform();
                    elementWait(listcountfield.get(0),3);
                } else {

                   ... ... ... ...

                 }
            */

            //Прокручиваем все наверх или вниз
            if (isVisible(uplrgbtn)) {
                while ((uplrgbtn.getAttribute("IsOffscreen").equalsIgnoreCase("False"))) {
                   Action moveup = action2.doubleClick(uplrgbtn).build();
                    moveup.perform();
                }
            Action moveup = action2.doubleClick(upbtn).build();
            moveup.perform();
            } else {
                //Если нет быстрой прокрутки и элементы расположены хаотично - придется много раз кликать вверх на кнопку
                //Примерно в n раза больше размера спиcка (задается параметром)
                // где n - за сколько кликов элемент проходит через видимую часть списка
                for (int i=0; i<n*list.size(); i++) {
                    Action moveup = action2.doubleClick(upbtn).build();
                    moveup.perform();
                }
                Action moveup = action2.doubleClick(upbtn).build();
                moveup.perform();
            }

            //Идем по списку групп счетчиков
            //Да, так, потому что они отображаются в другом порядке, нежели выдаются поиском
            //Прокручиваем поке не дойдем до конца списка (или не найдем ранее)
            //Если есть быстрая прокрутка - ориентируемся по ней, иначе только примерно
            if (isVisible(dwlrgbtn)) {
              while (!dwlrgbtn.getAttribute("IsOffscreen").equalsIgnoreCase("True")) {
                if ((isVisible(wgroup))&&(wgroup.getAttribute("IsOffscreen").equalsIgnoreCase("False"))&&(!(wgroup.getAttribute("ClickablePoint").equalsIgnoreCase("(null)")))) {
                    //Кликаем на группу
                    wgroup.click();
                    log.error("Элемент \""+element+"\" найден !");
                    return wgroup;
                } else {
                    //Скролл на 1 строку вниз
                    dwbtn.click();
                }
              }
            } else {
                //Если нет быстрой прокрутки и элементы расположены хаотично - придется много раз кликать вверх на кнопку
                //Примерно в n раза больше размера спиcка (задается параметром)
                // где n - за сколько кликов элемент проходит через видимую часть списка
               for (int i=0; i<n*list.size(); i++) {
                    if ((isVisible(wgroup))&&(wgroup.getAttribute("IsOffscreen").equalsIgnoreCase("False"))&&(!(wgroup.getAttribute("ClickablePoint").equalsIgnoreCase("(null)")))) {
                        //Кликаем на группу
                        wgroup.click();
                        log.error("Элемент \""+element+"\" найден !");
                        return wgroup;
                    } else {
                        //Скролл на 1 строку вниз
                        dwbtn.click();
                    }
                }
            }
        } else {
            //Кликаем на группу
            wgroup.click();
            return wgroup;
        }
        log.error("Группа НЕ была найдена !");
        return null;
    }

}
